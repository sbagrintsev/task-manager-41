package ru.tsc.bagrintsev.tm.api.model;

import org.jetbrains.annotations.NotNull;

public interface ICommand {

    @NotNull
    String getDescription();

    @NotNull
    String getName();

    @NotNull
    String getShortName();

}
