package ru.tsc.bagrintsev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Table(name = "m_task")
@NoArgsConstructor
public final class TaskDTO extends AbstractWBSModelDTO {

    @Nullable
    @Column(name = "project_id")
    private String projectId;

}
