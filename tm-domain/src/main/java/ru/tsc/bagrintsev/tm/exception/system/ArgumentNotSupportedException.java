package ru.tsc.bagrintsev.tm.exception.system;

import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException(@Nullable final String arg) {
        super(String.format("Error! Argument '%s' is not supported...", arg));
    }

}
