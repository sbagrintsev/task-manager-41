package ru.tsc.bagrintsev.tm.dto.request.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class ProjectClearRequest extends AbstractUserRequest {

    public ProjectClearRequest(@Nullable String token) {
        super(token);
    }

}
