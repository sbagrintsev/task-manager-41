package ru.tsc.bagrintsev.tm.exception.user;

import org.jetbrains.annotations.Nullable;

public class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException(@Nullable Throwable cause) {
        super(cause);
    }

    public AccessDeniedException() {
        super("Access denied! Authentication failed, check login or password...");
    }

}
