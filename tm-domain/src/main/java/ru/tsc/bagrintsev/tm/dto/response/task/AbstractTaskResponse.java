package ru.tsc.bagrintsev.tm.dto.response.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.response.AbstractResponse;
import ru.tsc.bagrintsev.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResponse {

    @Nullable
    private TaskDTO task;

}
