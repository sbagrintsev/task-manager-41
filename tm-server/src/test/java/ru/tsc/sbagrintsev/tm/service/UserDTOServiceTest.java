package ru.tsc.sbagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.dto.model.UserDTO;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectParameterNameException;
import ru.tsc.bagrintsev.tm.exception.user.AccessDeniedException;
import ru.tsc.bagrintsev.tm.exception.user.PasswordIsIncorrectException;
import ru.tsc.bagrintsev.tm.service.*;
import ru.tsc.bagrintsev.tm.util.HashUtil;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.security.GeneralSecurityException;

@Category(DBCategory.class)
public final class UserDTOServiceTest {

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private UserService userService;

    @Before
    public void setUp() {
        @NotNull final ConnectionService connectionService = new ConnectionService(propertyService);
        @NotNull final ProjectService projectService = new ProjectService(connectionService);
        @NotNull final TaskService taskService = new TaskService(connectionService);
        userService = new UserService(projectService, taskService, propertyService, connectionService);
    }

    @Test
    @Category(DBCategory.class)
    public void testAdd() throws AbstractException, GeneralSecurityException {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull final UserDTO user = userService.create("testLogin", "testPassword");
        Assert.assertFalse(userService.findAll().isEmpty());
        Assert.assertEquals(user, userService.findAll().get(0));
    }

    @Test(expected = PasswordIsIncorrectException.class)
    @Category(DBCategory.class)
    public void testCheckUser() throws AbstractException, GeneralSecurityException {
        userService.create("testLogin", "testPassword");
        Assert.assertNotNull(userService.checkUser("testLogin", "testPassword"));
        Assert.assertNotNull(userService.checkUser("testLogin", "wrongPassword"));
    }

    @Test(expected = AccessDeniedException.class)
    @Category(DBCategory.class)
    public void testCheckUserLocked() throws AbstractException, GeneralSecurityException {
        @NotNull final UserDTO user = userService.create("testLogin", "testPassword");
        Assert.assertNotNull(userService.checkUser("testLogin", "testPassword"));
        user.setLocked(true);
        Assert.assertNotNull(userService.checkUser("testLogin", "testPassword"));
    }

    @Test
    @Category(DBCategory.class)
    public void testClear() throws GeneralSecurityException, AbstractException {
        userService.create("testLogin", "testPassword");
        userService.create("testLogin2", "testPassword2");
        Assert.assertEquals(2, userService.findAll().size());
        userService.clearAll();
        Assert.assertEquals(0, userService.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void testCreate() throws AbstractException, GeneralSecurityException {
        userService.create("testLogin", "testPass");
        Assert.assertFalse(userService.findAll().isEmpty());
        Assert.assertEquals("testLogin", userService.findAll().get(0).getLogin());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAll() throws AbstractException, GeneralSecurityException {
        userService.create("testLogin1", "testPass1");
        userService.create("testLogin2", "testPass2");
        userService.create("testLogin3", "testPass3");
        Assert.assertEquals(3, userService.findAll().size());
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DBCategory.class)
    public void testFindByEmail() throws AbstractException, GeneralSecurityException {
        @NotNull final UserDTO user = userService.create("testLogin", "testPassword");
        user.setEmail("test@email.ru");
        Assert.assertEquals(user, userService.findByEmail("test@email.ru"));
        Assert.assertEquals(user, userService.findByEmail("wrong"));
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DBCategory.class)
    public void testFindByLogin() throws AbstractException, GeneralSecurityException {
        @NotNull final UserDTO user = userService.create("testLogin", "testPassword");
        Assert.assertEquals(user, userService.findByLogin("login1"));
        Assert.assertEquals(user, userService.findByLogin("wrong"));
    }

    @Test
    @Category(DBCategory.class)
    public void testFindOneById() throws AbstractException, GeneralSecurityException {
        @NotNull final UserDTO user = userService.create("testLogin", "testPassword");
        user.setId("id1");
        @NotNull final UserDTO user2 = userService.create("testLogin2", "testPassword2");
        user2.setId("id2");
        Assert.assertEquals("testLogin", userService.findOneById("id1").getLogin());
        Assert.assertEquals("testLogin2", userService.findOneById("id2").getLogin());
    }

    @Test
    @Category(DBCategory.class)
    public void testIsEmailExists() throws AbstractException, GeneralSecurityException {
        @NotNull final UserDTO user = userService.create("testLogin", "testPassword");
        user.setEmail("test@email.ru");
        Assert.assertTrue(userService.isEmailExists("test@email.ru"));
        Assert.assertFalse(userService.isEmailExists("wrong@email.ru"));
    }

    @Test
    @Category(DBCategory.class)
    public void testIsLoginExists() throws AbstractException, GeneralSecurityException {
        userService.create("testLogin", "testPassword");
        Assert.assertTrue(userService.isLoginExists("testLogin"));
        Assert.assertFalse(userService.isLoginExists("wrongLogin"));
    }

    @Test
    @Category(DBCategory.class)
    public void testLockByLogin() throws AbstractException, GeneralSecurityException {
        @NotNull final UserDTO user = userService.create("testLogin", "testPassword");
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin("testLogin");
        Assert.assertTrue(user.getLocked());
    }

    @Test
    @Category(DBCategory.class)
    public void testRemoveByLogin() throws AbstractException, GeneralSecurityException {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull final UserDTO user = userService.create("testLogin", "testPassword");
        Assert.assertNotNull(userService.findByLogin("testLogin"));
        Assert.assertEquals(user, userService.removeByLogin("testLogin"));
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test(expected = IncorrectParameterNameException.class)
    @Category(DBCategory.class)
    public void testSetParameter() throws AbstractException, GeneralSecurityException {
        @NotNull final UserDTO user1 = userService.create("testLogin", "testPassword");
        Assert.assertNull(user1.getEmail());
        userService.setParameter(user1, EntityField.EMAIL, "test@email.ru");
        Assert.assertEquals("test@email.ru", user1.getEmail());
        userService.setParameter(user1, EntityField.NAME, "error");
    }

    @Test
    @Category(DBCategory.class)
    public void testSetPassword() throws GeneralSecurityException, AbstractException {
        @NotNull final UserDTO user = userService.create("testLogin", "testPassword");
        user.setId("id1");
        @NotNull final Integer iterations = propertyService.getPasswordHashIterations();
        @NotNull final Integer keyLength = propertyService.getPasswordHashKeyLength();
        @Nullable final String expectedPassHash = HashUtil.generateHash("testPassword", user.getPasswordSalt(), iterations, keyLength);
        Assert.assertEquals(expectedPassHash, user.getPasswordHash());
        userService.setPassword("id1", "newPassword", "testPassword");
        @Nullable final String expectedNewPassHash = HashUtil.generateHash("newPassword", user.getPasswordSalt(), iterations, keyLength);
        Assert.assertEquals(expectedNewPassHash, user.getPasswordHash());
    }

    @Test
    @Category(DBCategory.class)
    public void testSetRole() throws AbstractException, GeneralSecurityException {
        @NotNull final UserDTO user = userService.create("testLogin", "testPassword");
        Assert.assertEquals(Role.REGULAR, user.getRole());
        userService.setRole("testLogin", Role.ADMIN);
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    @Category(DBCategory.class)
    public void testTotalCount() throws GeneralSecurityException, AbstractException {
        userService.create("testLogin", "testPassword");
        userService.create("testLogin2", "testPassword2");
        Assert.assertEquals(2, userService.totalCount());
    }

    @Test
    @Category(DBCategory.class)
    public void testUnlockByLogin() throws AbstractException, GeneralSecurityException {
        @NotNull final UserDTO user = userService.create("testLogin", "testPassword");
        userService.lockUserByLogin("testLogin");
        Assert.assertTrue(user.getLocked());
        userService.unlockUserByLogin("testLogin");
        Assert.assertFalse(user.getLocked());
    }

    @Test
    @Category(DBCategory.class)
    public void testUpdateUser() throws AbstractException, GeneralSecurityException {
        @NotNull final UserDTO user = userService.create("testLogin", "testPassword");
        user.setId("qwer");
        Assert.assertNull(user.getFirstName());
        Assert.assertNull(user.getMiddleName());
        Assert.assertNull(user.getLastName());
        userService.updateUser("qwer", "testFirst", "testLast", "testMiddle");
        Assert.assertEquals("testFirst", user.getFirstName());
        Assert.assertEquals("testLast", user.getLastName());
        Assert.assertEquals("testMiddle", user.getMiddleName());
    }

}
