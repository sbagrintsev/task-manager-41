package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.dto.model.ProjectDTO;

public interface IProjectService extends IUserOwnedService<ProjectDTO> {

}
