package ru.tsc.bagrintsev.tm.service;

import lombok.Getter;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.IUserRepository;
import ru.tsc.bagrintsev.tm.api.sevice.*;
import ru.tsc.bagrintsev.tm.dto.model.UserDTO;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.IncorrectRoleException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.EmailIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectParameterNameException;
import ru.tsc.bagrintsev.tm.exception.user.*;
import ru.tsc.bagrintsev.tm.util.HashUtil;

import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class UserService extends AbstractService<UserDTO, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @Getter
    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService,
            @NotNull final IPropertyService propertyService,
            @NotNull final IConnectionService connectionService
    ) {
        super(connectionService);
        this.projectService = projectService;
        this.taskService = taskService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public UserDTO checkUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws PasswordIsIncorrectException, AccessDeniedException, LoginIsIncorrectException, GeneralSecurityException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        if (password == null || password.isEmpty()) throw new PasswordIsIncorrectException();
        @NotNull final UserDTO user = findByLogin(login);
        if (user.getLocked()) throw new AccessDeniedException();
        @NotNull final Integer iterations = propertyService.getPasswordHashIterations();
        @NotNull final Integer keyLength = propertyService.getPasswordHashKeyLength();
        if (!(HashUtil.generateHash(password, user.getPasswordSalt(), iterations, keyLength)).equals(user.getPasswordHash())) {
            throw new PasswordIsIncorrectException();
        }
        return user;
    }

    @Override
    public void clearAll() {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.clearAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password
    ) throws GeneralSecurityException, PasswordIsIncorrectException, LoginIsIncorrectException, LoginAlreadyExistsException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        if (password == null || password.isEmpty()) throw new PasswordIsIncorrectException();
        @NotNull final Integer iterations = getPropertyService().getPasswordHashIterations();
        @NotNull final Integer keyLength = getPropertyService().getPasswordHashKeyLength();
        final byte @NotNull [] salt = HashUtil.generateSalt();
        @NotNull final String passwordHash = HashUtil.generateHash(password, salt, iterations, keyLength);
        @NotNull final SqlSession sqlSession = getSession();
        @NotNull UserDTO user = new UserDTO(login, passwordHash, salt);
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final List<UserDTO> result = repository.findAll();
            return (result == null) ? Collections.emptyList() : result;
        }
    }

    @NotNull
    @Override
    public UserDTO findByEmail(@Nullable final String email) throws EmailIsEmptyException, UserNotFoundException {
        if (email == null || email.isEmpty()) throw new EmailIsEmptyException();
        @Nullable UserDTO user;
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            user = repository.findByEmail(email);
            if (user == null) throw new UserNotFoundException();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO findByLogin(@Nullable final String login) throws LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        @Nullable UserDTO user;
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
        }
        return user;
    }

    @Override
    public @NotNull UserDTO findOneById(@Nullable String id) throws IdIsEmptyException, UserNotFoundException {
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        @Nullable UserDTO user;
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            user = repository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
        }
        return user;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        try {
            findByEmail(email);
        } catch (AbstractException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        try {
            findByLogin(login);
        } catch (AbstractException e) {
            return false;
        }
        return true;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final UserDTO user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            repository.setParameter(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public UserDTO removeByLogin(@Nullable final String login) throws LoginIsIncorrectException, IdIsEmptyException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        @Nullable UserDTO user;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            repository.removeByLogin(login);
            @NotNull final String userId = user.getId();
            taskService.clear(userId);
            projectService.clear(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Override
    public @NotNull Collection<UserDTO> set(@NotNull Collection<UserDTO> records) {
        if (records.isEmpty()) return records;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.addAll(records);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return records;
    }

    @NotNull
    @Override
    public UserDTO setParameter(
            @Nullable final UserDTO user,
            @NotNull final EntityField paramName,
            @Nullable final String paramValue
    ) throws EmailAlreadyExistsException, UserNotFoundException, IncorrectParameterNameException {
        if (user == null) throw new UserNotFoundException();
        if (EntityField.EMAIL.equals(paramName) && isEmailExists(paramValue)) {
            throw new EmailAlreadyExistsException(paramValue);
        }
        @Nullable UserDTO userResult;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            switch (paramName) {
                case EMAIL:
                    user.setEmail(paramValue);
                    break;
                case FIRST_NAME:
                    user.setFirstName(paramValue);
                    break;
                case MIDDLE_NAME:
                    user.setMiddleName(paramValue);
                    break;
                case LAST_NAME:
                    user.setLastName(paramValue);
                    break;
                case LOCKED:
                    user.setLocked("true".equals(paramValue));
                    break;
                default:
                    throw new IncorrectParameterNameException(paramName, "User");
            }
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.setParameter(user);
            sqlSession.commit();
            userResult = repository.findOneById(user.getId());
            if (userResult == null) throw new UserNotFoundException();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return userResult;
    }

    @NotNull
    @Override
    public UserDTO setPassword(
            @Nullable final String userId,
            @Nullable final String newPassword,
            @Nullable final String oldPassword
    ) throws GeneralSecurityException, PasswordIsIncorrectException, IdIsEmptyException, AccessDeniedException, LoginIsIncorrectException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (newPassword == null || newPassword.isEmpty()) throw new PasswordIsIncorrectException();
        if (oldPassword == null || oldPassword.isEmpty()) throw new PasswordIsIncorrectException();
        @NotNull final Integer iterations = getPropertyService().getPasswordHashIterations();
        @NotNull final Integer keyLength = getPropertyService().getPasswordHashKeyLength();
        final byte @NotNull [] salt = HashUtil.generateSalt();
        @NotNull final String passwordHash = HashUtil.generateHash(newPassword, salt, iterations, keyLength);
        @Nullable UserDTO user;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            user = repository.findOneById(userId);
            if (user == null) throw new UserNotFoundException();
            checkUser(user.getLogin(), oldPassword);
            repository.setUserPassword(user.getLogin(), passwordHash, salt);
            sqlSession.commit();
            user = repository.findOneById(userId);
            if (user == null) throw new UserNotFoundException();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO setRole(
            @Nullable final String login,
            @Nullable final Role role
    ) throws IncorrectRoleException, LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        if (role == null) throw new IncorrectRoleException();
        @Nullable UserDTO user;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.setRole(login, role);
            user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Override
    public long totalCount() {
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.totalCount();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws LoginIsIncorrectException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginIsIncorrectException();
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final UserDTO user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(false);
            repository.setParameter(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public UserDTO updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws IdIsEmptyException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        @Nullable UserDTO user;
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            user = repository.findOneById(userId);
            if (user == null) throw new UserNotFoundException();
            user.setFirstName(firstName);
            user.setMiddleName(middleName);
            user.setLastName(lastName);
            repository.setParameter(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

}
