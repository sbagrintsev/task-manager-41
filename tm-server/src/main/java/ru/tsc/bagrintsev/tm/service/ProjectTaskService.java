package ru.tsc.bagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectService;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectTaskService;
import ru.tsc.bagrintsev.tm.api.sevice.ITaskService;
import ru.tsc.bagrintsev.tm.dto.model.TaskDTO;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    public ProjectTaskService(
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public TaskDTO bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws IdIsEmptyException, ProjectNotFoundException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (projectId == null || projectId.isEmpty())
            throw new IdIsEmptyException(EntityField.PROJECT_ID.getDisplayName());
        if (taskId == null || taskId.isEmpty()) throw new IdIsEmptyException(EntityField.TASK_ID.getDisplayName());
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (!taskService.existsById(userId, taskId)) throw new TaskNotFoundException();
        return taskService.setProjectId(userId, taskId, projectId);
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws IdIsEmptyException, ProjectNotFoundException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (projectId == null || projectId.isEmpty())
            throw new IdIsEmptyException(EntityField.PROJECT_ID.getDisplayName());
        @NotNull final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        for (TaskDTO task : tasks) {
            taskService.removeById(userId, task.getId());
        }
        projectService.removeById(userId, projectId);
    }

    @Override
    public TaskDTO unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) throws IdIsEmptyException, TaskNotFoundException {
        if (userId == null || userId.isEmpty()) throw new IdIsEmptyException(EntityField.USER_ID.getDisplayName());
        if (taskId == null || taskId.isEmpty()) throw new IdIsEmptyException(EntityField.TASK_ID.getDisplayName());
        return taskService.setProjectId(userId, taskId, null);
    }

}
