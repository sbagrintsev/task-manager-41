package ru.tsc.bagrintsev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.component.Bootstrap;

/**
 * @author Sergey Bagrintsev
 * @version 1.37.0
 */

public final class Application {

    public static void main(@Nullable final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
