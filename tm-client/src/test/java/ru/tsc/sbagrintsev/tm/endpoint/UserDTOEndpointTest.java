package ru.tsc.sbagrintsev.tm.endpoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.bagrintsev.tm.api.endpoint.IUserEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.dto.request.user.*;
import ru.tsc.bagrintsev.tm.dto.response.user.*;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.dto.model.UserDTO;
import ru.tsc.bagrintsev.tm.service.PropertyService;
import ru.tsc.sbagrintsev.tm.marker.SoapCategory;

import java.security.GeneralSecurityException;

@Category(SoapCategory.class)
public class UserDTOEndpointTest {

    @NotNull
    private final String JUNIT = "junit";

    @NotNull
    private final String ADMIN = "admin";

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());

    @Nullable
    private String tokenAdmin;

    @Before
    public void setUp() throws AbstractException, GeneralSecurityException, JsonProcessingException {
        @NotNull final UserSignInResponse responseAdmin = authEndpoint.signIn(new UserSignInRequest(ADMIN, ADMIN));
        tokenAdmin = responseAdmin.getToken();
        userEndpoint.signUp(new UserSignUpRequest(JUNIT, JUNIT, JUNIT + "@email.com"));
    }

    @After
    public void tearDown() {
        try {
            userEndpoint.remove(new UserRemoveRequest(tokenAdmin, JUNIT));
        } catch (Exception e) {

        }
    }

    @Test
    public void testChangePassword() throws AbstractException, GeneralSecurityException, JsonProcessingException {
        @NotNull UserSignInResponse response = authEndpoint.signIn((new UserSignInRequest(JUNIT, JUNIT)));
        Assert.assertNotNull(response);
        @Nullable final String token = response.getToken();
        Assert.assertNotNull(token);
        @NotNull final UserChangePasswordResponse response1 = userEndpoint.changePassword(
                new UserChangePasswordRequest(token, JUNIT, "newJunit")
        );
        Assert.assertNotNull(response1);
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.signIn(new UserSignInRequest(JUNIT, JUNIT))
        );
        response = authEndpoint.signIn(new UserSignInRequest(JUNIT, "newJunit"));
        Assert.assertNotNull(response);
    }

    @Test
    public void testLock() throws AbstractException, GeneralSecurityException, JsonProcessingException {
        @NotNull final UserSignInResponse response = authEndpoint.signIn((new UserSignInRequest(JUNIT, JUNIT)));
        Assert.assertNotNull(response);
        userEndpoint.lock(new UserLockRequest(tokenAdmin, JUNIT));
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.signIn(new UserSignInRequest(JUNIT, JUNIT))
        );
    }

    @Test
    public void testRemove() throws AbstractException, GeneralSecurityException, JsonProcessingException {
        @NotNull final UserSignInResponse response = authEndpoint.signIn((new UserSignInRequest(JUNIT, JUNIT)));
        Assert.assertNotNull(response);
        userEndpoint.remove(new UserRemoveRequest(tokenAdmin, JUNIT));
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.signIn(new UserSignInRequest(JUNIT, JUNIT))
        );
    }

    @Test
    public void testSetRole() throws AbstractException, GeneralSecurityException, JsonProcessingException {
        @NotNull final UserSignInResponse response = authEndpoint.signIn((new UserSignInRequest(JUNIT, JUNIT)));
        Assert.assertNotNull(response);
        @Nullable final String token = response.getToken();
        Assert.assertNotNull(token);
        @NotNull UserViewProfileResponse response1 = authEndpoint.viewProfile(new UserViewProfileRequest(token));
        Assert.assertNotNull(response1);
        @Nullable UserDTO user = response1.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(Role.REGULAR, user.getRole());
        @NotNull final UserSetRoleResponse response2 = userEndpoint.setRole(new UserSetRoleRequest(tokenAdmin, JUNIT, Role.ADMIN));
        Assert.assertNotNull(response2);
        user = response2.getUser();
        Assert.assertNotNull(user);
        response1 = authEndpoint.viewProfile(new UserViewProfileRequest(token));
        Assert.assertNotNull(response1);
        user = response1.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void testSignUp() {
        @NotNull final UserSignUpResponse response = userEndpoint.signUp(
                new UserSignUpRequest("jTest", "jTest", "jTest@email.com")
        );
        Assert.assertNotNull(response);
        @Nullable final UserDTO user = response.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals("jTest", user.getLogin());
        Assert.assertEquals("jTest@email.com", user.getEmail());
        userEndpoint.remove(new UserRemoveRequest(tokenAdmin, "jTest"));
    }

    @Test
    public void testUnlock() throws AbstractException, GeneralSecurityException, JsonProcessingException {
        userEndpoint.lock(new UserLockRequest(tokenAdmin, JUNIT));
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.signIn(new UserSignInRequest(JUNIT, JUNIT))
        );
        userEndpoint.unlock(new UserUnlockRequest(tokenAdmin, JUNIT));
        @NotNull final UserSignInResponse response = authEndpoint.signIn((new UserSignInRequest(JUNIT, JUNIT)));
        Assert.assertNotNull(response);
    }

    @Test
    public void testUpdateProfile() throws AbstractException, GeneralSecurityException, JsonProcessingException {
        @NotNull final UserSignInResponse response = authEndpoint.signIn((new UserSignInRequest(JUNIT, JUNIT)));
        Assert.assertNotNull(response);
        @Nullable final String token = response.getToken();
        Assert.assertNotNull(token);
        @NotNull UserViewProfileResponse response1 = authEndpoint.viewProfile(new UserViewProfileRequest(token));
        Assert.assertNotNull(response1);
        @Nullable UserDTO user = response1.getUser();
        Assert.assertNotNull(user);
        Assert.assertNull(user.getLastName());
        @NotNull UserUpdateProfileResponse response2 = userEndpoint.updateProfile(
                new UserUpdateProfileRequest(token, "first", "middle", "last")
        );
        Assert.assertNotNull(response2);
        user = response2.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals("last", user.getLastName());
    }

}
