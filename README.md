# TASK MANAGER

### DEVELOPER INFO

* **NAME**: Sergey Bagrintsev

* **E-MAIL**: sbagrintsev@t1-consulting.ru

* **E-MAIL**: svbagrincev@dev.vtb.ru

### SOFTWARE

* OpenJDK 8

* Intellij Idea

* Windows 10

### HARDWARE

* RAM: 16Gb

* CPU: Intel Core i5-8400T

* SSD: 256Gb

### BUILD APPLICATION

```bash
mvn clean install
```

### RUN APPLICATION

```bash
java -jar ./task-manager.jar
```
